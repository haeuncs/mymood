//
//  realmModel.swift
//  Mymood
//
//  Created by LEE HAEUN on 2019. 5. 19..
//  Copyright © 2019년 LEE HAEUN. All rights reserved.
//

import Foundation
import RealmSwift


class DiaryData: Object {
    @objc dynamic var date : String?
    @objc dynamic var diary : String?
}
class Data: Object {
    let data = List<DiaryData>()
}
